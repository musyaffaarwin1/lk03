public class Pertambahan extends Kalkulator {
    public void setOperan(double operand1, double operand2) {
        this.operand1 = operand1;
        this.operand2 = operand2;
    }

    public double hitung() {
        return operand1 + operand2;
    }
}