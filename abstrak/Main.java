/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas kalkulator, pertambahan, dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa abstract class akan lebih cocok digunakan daripada interface?
 * "Jawab disini..." 
 * Jawaban = 
 * Di konteks kalkulator, abstract class akan lebih cocok digunakan 
 * jika ada beberapa method yg diimplementasikan dgn isi tertentu.
 * method seperti setOperan() dan hitung() perlu diimplementasikan dengan 
 * isi tertentu. maka dari itu lebih cocok untuk menggunakan abstract
 * dalam class Kalkulator juga menyimpan atribut operand1 dan operand2 yg digunakan
 * pd kalkulator sehingga subclassnya hanya perlu meng-extend class Kalkulator
 * untuk menggunakan atribut tersebut             
 */

 import java.util.*;

 public class Main {
     public static void main(String[] args) {
         Scanner scanner = new Scanner(System.in);
 
         System.out.print("Masukan operan 1: ");
         double operand1 = scanner.nextDouble();
 
         System.out.print("Masukan operan 2: ");
         double operand2 = scanner.nextDouble();
 
         System.out.print("Masukan operator (+ atau -): ");
         String operator = scanner.next();
         scanner.close();
 
         Kalkulator kalkulator;
         if (operator.equals("+")) {
             kalkulator = new Pertambahan();
         } else if (operator.equals("-")) {
             kalkulator = new Pengurangan();
         } else {
             System.out.println("Operator tidak valid!");
             return;
         }
 
         kalkulator.setOperan(operand1, operand2);
         double result = kalkulator.hitung();
         System.out.println("Hasil: " + result);
     }
 }
 